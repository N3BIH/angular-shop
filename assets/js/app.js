angular.module('angularShop', [])



    .factory('Cart', function() {
        var items = [];
        return {

            //Artikel laden
            getItems: function() {
                return items;
            },

            //Artikel hinzufügen
            addArticle: function(article) {
                items.push(article);
            },

            //Artikel löschen
            removeArticle: function(article) {
                items.splice(article, 1);
            },
            sum: function() {
                return items.reduce(function(total, article) {
                    return total + article.price;
                }, 0);
            }
        };
    })


    //Warenkorb
    .controller('ArticlesCtrl', function($scope, $http, Cart){
        $scope.cart = Cart;
        $http.get('articles.json').then(function(articlesResponse) {
            $scope.articles = articlesResponse.data;
    });




    })

    .controller('CartCtrl', function($scope, Cart){
        $scope.cart = Cart;


});

